# display_buffer

`display_buffer` is a `no_std` helper library to make implementing
[`core::fmt::Display`][1] easier.

While implementing `core::fmt::Display` for a new type seems quite easy, it is
actually quite complex to support [formatting parameters][2] correctly.
Fortunately most of this complexity can be avoided by deferring to the
implementation for `str` in the core library.  However the naive way of doing
this is to create a [`String`][3] using [`format!`][4] which allocates space on
the heap.  For a lot of types the string representation is not that long and so
it could easily be formatted into a buffer on the stack.

For types which can be formatted using a single [`core::write!`][5], the `fmt!`
helper macro is provided.

[1]: https://doc.rust-lang.org/core/fmt/trait.Display.html
[2]: https://doc.rust-lang.org/std/fmt/#formatting-parameters
[3]: https://doc.rust-lang.org/alloc/string/struct.String.html
[4]: https://doc.rust-lang.org/alloc/macro.format.html
[5]: https://doc.rust-lang.org/core/macro.write.html
