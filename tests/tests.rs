use core::fmt::Write;
use display_buffer::DisplayBuffer;

#[test]
fn write_empty_str() {
    let mut buf = DisplayBuffer::<8>::new();
    assert!(buf.write_str("").is_ok());
    assert_eq!(buf.as_str(), "");
}

#[test]
fn write_multiple_strs() {
    let mut buf = DisplayBuffer::<8>::new();
    assert!(buf.write_str("abc").is_ok());
    assert_eq!(buf.as_str(), "abc");
    assert!(buf.write_str("def").is_ok());
    assert_eq!(buf.as_str(), "abcdef");
}

#[test]
fn write_too_much() {
    let mut buf = DisplayBuffer::<8>::new();
    assert!(buf.write_str("abc").is_ok());
    assert_eq!(buf.as_str(), "abc");
    assert!(buf.write_str("def").is_ok());
    assert_eq!(buf.as_str(), "abcdef");
    assert!(buf.write_str("ghi").is_err());
    assert_eq!(buf.as_str(), "abcdef");
}

#[test]
fn write_char() {
    let mut buf = DisplayBuffer::<16>::new();
    assert!(buf.write_char('a').is_ok());
    assert_eq!(buf.as_str(), "a");
    assert_eq!(buf.len(), 1);
    assert!(buf.write_char('µ').is_ok());
    assert_eq!(buf.as_str(), "aµ");
    assert_eq!(buf.len(), 3);
    assert!(buf.write_char('€').is_ok());
    assert_eq!(buf.as_str(), "aµ€");
    assert_eq!(buf.len(), 6);
    assert!(buf.write_char('💩').is_ok());
    assert_eq!(buf.as_str(), "aµ€💩");
    assert_eq!(buf.len(), 10);
}

#[test]
fn write_1_byte_char() {
    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_str("123").is_ok());
    assert!(buf.write_char('a').is_ok());
    assert_eq!(buf.as_str(), "123a");

    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_str("1234").is_ok());
    assert!(buf.write_char('a').is_err());
    assert_eq!(buf.as_str(), "1234");
}

#[test]
fn write_2_byte_char() {
    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_str("12").is_ok());
    assert!(buf.write_char('µ').is_ok());
    assert_eq!(buf.as_str(), "12µ");

    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_str("123").is_ok());
    assert!(buf.write_char('µ').is_err());
    assert_eq!(buf.as_str(), "123");

    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_str("1234").is_ok());
    assert!(buf.write_char('µ').is_err());
    assert_eq!(buf.as_str(), "1234");
}

#[test]
fn write_3_byte_char() {
    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_str("1").is_ok());
    assert!(buf.write_char('€').is_ok());
    assert_eq!(buf.as_str(), "1€");

    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_str("12").is_ok());
    assert!(buf.write_char('€').is_err());
    assert_eq!(buf.as_str(), "12");

    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_str("123").is_ok());
    assert!(buf.write_char('€').is_err());
    assert_eq!(buf.as_str(), "123");

    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_str("1234").is_ok());
    assert!(buf.write_char('€').is_err());
    assert_eq!(buf.as_str(), "1234");
}

#[test]
fn write_4_byte_char() {
    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_char('💩').is_ok());
    assert_eq!(buf.as_str(), "💩");

    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_str("1").is_ok());
    assert!(buf.write_char('💩').is_err());
    assert_eq!(buf.as_str(), "1");

    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_str("12").is_ok());
    assert!(buf.write_char('💩').is_err());
    assert_eq!(buf.as_str(), "12");

    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_str("123").is_ok());
    assert!(buf.write_char('💩').is_err());
    assert_eq!(buf.as_str(), "123");

    let mut buf = DisplayBuffer::<4>::new();
    assert!(buf.write_str("1234").is_ok());
    assert!(buf.write_char('💩').is_err());
    assert_eq!(buf.as_str(), "1234");
}
